<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\RegisterForm;
use app\components\Util;
use app\components\NoteConstantes;

class SiteController extends Controller{
    
    use Util;
    
    /* VARIABLES LOCALES */
    private $_response = null;
    
    /* METODOS DE CLASE */
    
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    
    public function actions(){
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    
    public function actionIndex(){
        $user = null;
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            $obj = Yii::$app->request->post();
            $user = array(
                "email" => $obj["LoginForm"]["username"],
                "password" => $obj["LoginForm"]["password"],
            );
            $this->_response = $this->callDiana("post", "core/login", json_encode($user));
            if(!isset($this->_response["data"]["error"])){
                $this->_userSession = Yii::$app->session;
                $this->_userSession->set("user",$this->_response["data"]);
                return $this->redirect(['home/index']);
            }else{
                Yii::$app->session->setFlash('loginError');
                return $this->render('home',['model' => $model]);
            }
        }else{
            return $this->render('home',['model' => $model]);
        }
    }
    
    public function actionRegister(){
        $register = new RegisterForm();
        if ($register->load(Yii::$app->request->post())) {
            $obj = Yii::$app->request->post();
            $user = array(
                "email" => $obj["RegisterForm"]["email"],
                "password" => $obj["RegisterForm"]["password"],
                "refname" => $obj["RegisterForm"]["firstName"]." ".$obj["RegisterForm"]["lastName"],
                "bio" => "Lector",
                "type" => "redactor",
                "role" => array(
                    "id" => 2
                )
            );
            $this->_response = $this->callDiana("post", "redactor", json_encode($user));
            if(!isset($this->_response["error"])){
                $register = new RegisterForm();
                Yii::$app->session->setFlash('registerSuccess');
                return $this->render('register',['register' => $register]);
            }else{
                Yii::$app->session->setFlash('registerError');
                return $this->render('register',['register' => $register , "error" => $this->_response["error"]]);
            }
        }else{
            return $this->render('register',['register' => $register]);
        }
    }
    
    public function actionCallSocial(){
        
    }
    
    public function actionLogin(){
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }    

    public function actionLogout(){
        Yii::$app->user->logout();
        return $this->goHome();
    }

    public function actionContact(){
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');
            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionAbout(){
        return $this->render('about');
    }
    
}
