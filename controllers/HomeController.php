<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use app\components\Util;

class HomeController extends Controller{
    
    use Util;
    
    private $_response = null;
    
    public function actionIndex(){
        $this->_userSession = Yii::$app->session;
        $this->_response = $this->callDiana("get", "postcreator?author=41", "");
        return $this->render('index');
    }
    
}
