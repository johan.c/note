<?php

namespace app\components;

use Yii;
use yii\httpclient\Client;
use yii\helpers\Json;

trait Util {
    
    public $_userSession = null;

    protected function callDiana($method, $resource, $content) {
        $client = new Client([
            'baseUrl' => Yii::$app->params["dianaUrl"] . '/api/v2/' . Yii::$app->params["UIID"],
            'requestConfig' => ['format' => Client::FORMAT_JSON],
            'responseConfig' => ['format' => Client::FORMAT_JSON],
        ]);
        $response = $client->createRequest()->setMethod($method)->setUrl($resource)
                            ->setHeaders(['Authorization' => 'apikey ' . Yii::$app->params["apikey"]])
                            ->addHeaders(['content-type' => 'application/json'])
                            ->setContent($content)
                            ->setData(['how' => ['p_list'], 'query_string' => 'Yii', 'how' => ['p_list']])
                            ->send();
        /*
        if (!$response->isOk) {
            error_log("Error al consumir web service diana-digital: método:[$method] | recurso [$resource]");
            $response = Json::decode($response->content);
        }else {
            $response = Json::decode($response->content);
        }
        */
        $response = Json::decode($response->content);
        return $response;
    }

}
