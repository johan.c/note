<?php

namespace app\models;

use Yii;
use yii\base\Model;

class RegisterForm extends Model {
    
    public $firstName;
    public $lastName;
    public $typeDoc;
    public $doc;
    public $telephone;
    public $email;
    public $password;
    
    public function rules(){
        return [
            ['firstName', 'required', 'message' => 'Nombre es un campo obligatorio'],
            ['password', 'required', 'message' => 'La contraseña es un campo requerido'],
            ['lastName', 'required', 'message' => 'Apellido es un campo obligatorio'],
            ['email', 'required', 'message' => 'Email es un campo obligatorio'],
            ['email', 'email', 'message' => 'Introduzca un email válido'],
            ['typeDoc', 'required', 'message' => 'Tipo de Documento es un campo obligatorio'],
            ['doc', 'required', 'message' => 'N° de Documento es un campo obligatorio'],
            ['doc','match', 'pattern'=>'/^[0-9]+$/', 'message' => 'N° de Documento inválido.'],
            ['telephone', 'required', 'message' => 'Teléfono es un campo obligatorio'],
            ['telephone','match', 'pattern'=>'/^[0-9]+$/', 'message' => 'Teléfono inválido.'],
        ];
    }

    public function attributeLabels(){
        return [
            'firstName' => 'Nombre',
            'lastName' => 'Apellido',
            'email' => 'Email',
            'typeDoc' => 'Tipo de Documento',
            'doc' => 'N° de Documento',
            'telephone' => 'Teléfono',
        ];
    }

}
