<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>
<!-- MODAL REGISTER AUTHOR -->
<div class="modal fade" id="register_form" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Registrate</h4>
        </div>
        <div class="modal-body">
            <?php
                $formRegister = ActiveForm::begin(['id' => 'register-form', 'layout' => 'horizontal', 'action' => Url::to(['site/index']),
                                'fieldConfig' => [
                                    'template' => "<div class=\"row\"><div class=\"col-md-6\"><div class=\"form-group\">{label}{input}{error}</div></div></div>",
                                    'labelOptions' => ['class' => 'col-lg-1 control-label'],],
                ]);
            ?>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary">Registrar</button>  
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        </div>
      </div>
    </div>
  </div>