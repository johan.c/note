<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Note';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="container" >
    <div class="row" >
        <div class="col-md-12 col-xs-12 col-lg-12" >
            <div class="col-md-6 col-xs-6 col-lg-6" >
                <br/><br/>
                <h1>Fútbol Social</h1>
                <p style="text-align: justify;" >
                    Lorem Ipsum es simplemente el texto de relleno de las imprentas y archivos de texto. 
                    Lorem Ipsum ha sido el texto de relleno estándar de las industrias desde el año 1500, 
                    cuando un impresor (N. del T. persona que se dedica a la imprenta) desconocido usó una 
                    galería de textos y los mezcló de tal manera que logró hacer un libro de textos especimen. 
                    No sólo sobrevivió 500 años, sino que tambien ingresó como texto de relleno en documentos 
                    electrónicos, quedando esencialmente igual al original. Fue popularizado en los 60s con la
                    creación de las hojas "Letraset", las cuales contenian pasajes de Lorem Ipsum, y más 
                    recientemente con software de autoedición, como por ejemplo Aldus PageMaker, el cual 
                    incluye versiones de Lorem Ipsum.
                </p>
            </div>
            <div class="col-md-6 col-xs-6 col-lg-1" ></div>
            <div class="col-md-6 col-xs-6 col-lg-5" >
                <br/><br/>
                <?php
                    $form = ActiveForm::begin(['id' => 'login-form', 'layout' => 'horizontal', 'action' => Url::to(['site/index']),
                                'fieldConfig' => [
                                    'template' => "<div class=\"form-group\">{label}{input}{error}</div>",
                                    'labelOptions' => ['class' => 'col-lg-1 control-label'],],
                    ]);
                ?>
                <legend>Iniciar Sesión</legend>
                <?= $form->field($model, 'username')->textInput() ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?php if (Yii::$app->session->hasFlash('loginError')) { ?>
                    <div class="alert alert-danger">
                        Usuario o contraseña incorrectos.
                    </div>
                <?php } ?>
                <?= Html::submitButton('Iniciar Sesión', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                    &nbsp;&nbsp;
                    <a href="#" data-toggle="modal" data-target="#reset_form">Olvidaste tu contraseña?</a>
                    &nbsp;&nbsp;
                    <a href="<?php echo Url::to(['site/register']); ?>">Registrate</a>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <div class="row" >
        <?php echo $this->render('/partial/_modal.php'); ?>
    </div>
</div>