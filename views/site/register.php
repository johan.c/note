<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

?>
<div class="container">
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
             <?php
                $form = ActiveForm::begin(['id' => 'register-form', 'layout' => 'horizontal', 'action' => Url::to(['site/register']),
                                'fieldConfig' => [
                                    'template' => "<div class=\"row\"><div class=\"col-md-6\"><div class=\"form-group\">{label}{input}{error}</div></div></div>",
                                    'labelOptions' => ['class' => 'col-lg-1 control-label'],],
                ]);
            ?>
            <?= $form->field($register, 'email')->textInput() ?>
            <?= $form->field($register, 'password')->passwordInput() ?>
            <?= $form->field($register, 'firstName')->textInput() ?>
            <?= $form->field($register, 'lastName')->textInput() ?>
            <?php if (Yii::$app->session->hasFlash('registerError')) { ?>
                <div class="alert alert-danger"><?php echo $error[0]; ?></div>
            <?php }else if (Yii::$app->session->hasFlash('registerSuccess')) { ?>
                <div class="alert alert-success">Usuario creado correctamente</div>
            <?php } ?>    
            <?= Html::submitButton('Registrate', ['class' => 'btn btn-primary', 'name' => 'register-button']) ?>
            &nbsp;&nbsp;
            <a href="#" id="login" class="btn btn-primary">Inicia con Facebook</a>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-2"></div>
    </div>
</div>